from picamera import PiCamera
from time import sleep
import time

camera = PiCamera()

camera.start_preview()

# Wait for light to adjust
sleep(3)

# Set a timestamp
timestamp = int(time.time())

# Add a filenamewith a timestamp
filename = "img_{}.jpg".format(str(timestamp))
camera.capture(filename)

camera.stop_preview()

print 'File has been stored as {}'.format(filename)
