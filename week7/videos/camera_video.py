from picamera import PiCamera
from time import sleep
import time

camera = PiCamera()

camera.start_preview()

# Add a filename with a timestamp
timestamp = int(time.time())
filename = "vieo_{}.h264".format(str(timestamp))
camera.start_recording(filename)
sleep(10)
camera.stop_recording()
camera.stop_preview()

print 'File has been stored as: {}'.format(filename)
